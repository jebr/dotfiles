syntax enable
set enc=utf-8
set ignorecase
set smartcase
set smartindent
set smarttab
set expandtab
set ts=2
set softtabstop=2
set shiftwidth=2
set mouse-=a
set nowrap
set nomodeline
set noerrorbells
set laststatus=2
set statusline=[%n]\ %<%f%m%r\ %w\ %y\ \ <%{&fileformat}>%=[%o]\ %l,%c%V\/%L\ \ %P
set ruler
