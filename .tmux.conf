# command prefix (like screen)
set -g prefix C-a
bind C-a send-prefix

# basic settings
set-window-option -g mode-keys vi # vi key
set-option -g status-keys vi
set-window-option -g utf8 on # utf8 support

# copy mode to escape key
unbind [
bind Escape copy-mode

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf \; display-message "Config reloaded!"

# splitting and cycling
set-option -g mouse-select-pane off
unbind %
bind + split-window -h # horizontal split
unbind '"'
bind _ split-window -v # vertical split
bind C-j previous-window
bind C-k next-window

# window title
set-option -g set-titles on
set-option -g set-titles-string '#S:#I.#P #W' # window number,program name,active (or not)
set-window-option -g automatic-rename on # auto name

# messages
#set-window-option -g mode-bg magenta
#set-window-option -g mode-fg black
#set-option -g message-bg magenta
#set-option -g message-fg black

# No visual activity
set -g visual-activity off
set -g visual-bell off

#next tab
bind-key -n C-right next

#previous tab
bind-key -n C-left prev

# panes:
set-option -g pane-border-fg white
set-option -g pane-border-bg default
set-option -g pane-active-border-fg blue
set-option -g pane-active-border-bg default

# status bar
set -g status-interval 1
set -g status-justify centre # center align window list
set -g status-left-length 20
set -g status-right-length 140
set -g status-bg black
set -g status-left '#[fg=green]#H #[fg=white]• #[fg=green]#(uname -r | cut -c 1-6)#[default]'
set -g status-right '#[fg=blue,dim,bg=default]#(uptime | cut -f 4-5 -d " " | cut -f 1 -d ",") #[fg=white,bg=default]%a %k:%M #[default] #[fg=blue]%Y-%m-%d'

if-shell '\( #{$TMUX_VERSION_MAJOR} -eq 2 -a #{$TMUX_VERSION_MINOR} -lt 2\) -o #{$TMUX_VERSION_MAJOR} -le 1' 'set-option -g status-utf8 on'

# default window title colors
set-window-option -g window-status-fg colour244  # base0
set-window-option -g window-status-bg default

# active window title colors
set-window-option -g window-status-current-fg colour63  # blue
set-window-option -g window-status-current-bg default


# clock
set-window-option -g clock-mode-colour green
set-window-option -g clock-mode-style 24
